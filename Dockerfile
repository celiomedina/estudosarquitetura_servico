FROM mcr.microsoft.com/dotnet/core/sdk:2.1 AS build
WORKDIR /app

COPY *.sln ./
COPY ./src/*/*.csproj ./src/
RUN for file in src/*/*.csproj; do filename=$(basename -s .csproj $file); filepath="src/$filename"; mkdir $filepath; mv $file $filepath; done
RUN dotnet restore

COPY ./src ./src
RUN dotnet publish ./src/WsApi/ -c Release -o out --no-restore

FROM mcr.microsoft.com/dotnet/core/aspnet:2.1 AS runtime
WORKDIR /app
COPY --from=build /app/src/WsApi/out .
EXPOSE 80
ENTRYPOINT [ "dotnet", "WsApi.dll" ]
