﻿using Application.Dtos.Request;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IControleTransacoesApplicationService
    {
        Task<bool> Saque(TransacaoRequest request);

        Task<bool> Deposito(TransacaoRequest request);
    }
}
