﻿using Application.Dtos.Request;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IControleAcoesApplicationService
    {
        Task<bool> AcaoCompra(AcaoCompraRequest request);

        Task<bool> AcaoVenda(AcaoVendaRequest request);
    }
}
