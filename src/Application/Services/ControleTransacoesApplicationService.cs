﻿using Application.Dtos.Request;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ControleTransacoesApplicationService : IControleTransacoesApplicationService
    {
        private readonly IControleTransacoesDomainService _domainService;
        private readonly IMapper _mapper;

        public ControleTransacoesApplicationService(IControleTransacoesDomainService domainService, IMapper mapper)
        {
            _domainService = domainService;
            _mapper = mapper;
        }

        Task<bool> IControleTransacoesApplicationService.Saque(TransacaoRequest request)
        {
            return _domainService.Saque(_mapper.Map<Transacao>(request));
        }

        Task<bool> IControleTransacoesApplicationService.Deposito(TransacaoRequest request)
        {
            return _domainService.Deposito(_mapper.Map<Transacao>(request));
        }
    }
}
