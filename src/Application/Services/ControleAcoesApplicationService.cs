﻿using Application.Dtos.Request;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Processos
{
    public class ControleAcoesApplicationService : IControleAcoesApplicationService
    {
        private readonly IControleAcoesDomainService _domainService;
        private readonly IMapper _mapper;

        public ControleAcoesApplicationService(IControleAcoesDomainService domainService, IMapper mapper)
        {
            _domainService = domainService;
            _mapper = mapper;
        }

        public Task<bool> AcaoCompra(AcaoCompraRequest request)
        {
            return _domainService.AcaoCompra(_mapper.Map<AcaoCompraCliente>(request));
        }

        public Task<bool> AcaoVenda(AcaoVendaRequest request)
        {
            return _domainService.AcaoVenda(_mapper.Map<AcaoVendaCliente>(request));
        }
    }
}
