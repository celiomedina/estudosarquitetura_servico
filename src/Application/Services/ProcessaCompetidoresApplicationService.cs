﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Domain.AppSettings;
using Domain.Exceptions;
using Microsoft.Extensions.Logging;

namespace Application.Processos
{
    //public class ProcessaCompetidoresApplicationService : IProcessaCompetidoresService
    //{
    //    private readonly IProcessaArquivoInfrastructure _processaArquivos;
    //    private readonly IConfiguration _config;
    //    private readonly IOptions<ArquivoCompetidoresSettings> _options;
    //    private readonly ILogger<ProcessaCompetidoresService> _log;
    //    private readonly int maxLenght = 30;

    //    public ProcessaCompetidoresService(IProcessaArquivoInfrastructure processaArquivos, IConfiguration config, IOptions<ArquivoCompetidoresSettings> options, ILogger<ProcessaCompetidoresService> log)
    //    {
    //        _processaArquivos = processaArquivos;
    //        _config = config;
    //        _options = options;
    //        _log = log;
    //    }
    //    public bool FileProccess()
    //    {
    //        ValidaPercentualCompeticao();

    //        var listLinesCompetidores = _processaArquivos.ReadFileERP(_options.Value.pathReadERPCompetidores);

    //        var listCompetidoresFormatada = GetCompetidores(listLinesCompetidores);

    //        var listSerializes = Newtonsoft.Json.JsonConvert.SerializeObject(listCompetidoresFormatada);

    //        _processaArquivos.WriteFileFullText($"{ _options.Value.pathWriteJsonCompetidores}{DateTime.Now.ToFileTime().ToString()}.json", listSerializes);

    //        var listCompetidoresComRanking = CalculaRanking(listCompetidoresFormatada);

    //        ExibeCompetidoresPorNome(listCompetidoresComRanking);

    //        ExibeCompetidoresPorRanking(listCompetidoresComRanking);

    //        ExibeCompetidoresPorRegiaoAgrupados(listCompetidoresComRanking);

    //        ExibeQuantidadeCompetidores(listCompetidoresComRanking);

    //        Console.ReadLine();

    //        return true;
    //    }

    //    public void ExibeCompetidoresPorNome(List<Competidor> listCompetidor)
    //    {
    //        Console.WriteLine("*********** Exibe Competidores por Nome *****************");
    //        foreach (var item in listCompetidor.OrderBy(c => c.Nome))
    //        {
    //            Console.WriteLine($"{ScapeGenerate(item.Nome)} | {CalculaIdade(item.DataNascimento)} anos | Pontuação: {item.Pontos} | Posição Ranking: {item.PosicaoRanking}");
    //        }
    //        Console.WriteLine("*********** FIM *****************");
    //    }

    //    public void ExibeCompetidoresPorRanking(List<Competidor> listCompetidor)
    //    {
    //        Console.WriteLine("*********** Exibe Competidores por Ranking *****************");
    //        foreach (var item in listCompetidor.Where(c => c.Pontos > 10000).OrderBy(c => c.PosicaoRanking))
    //        {
    //            Console.WriteLine($"{ScapeGenerate(item.Nome)} | {CalculaIdade(item.DataNascimento)} anos | Pontuação: {item.Pontos} | Ouro: {item.MedalhasOuro} | Prata: {item.MedalhasPrata} | Bronze: {item.MedalhasBronze} | Posição Ranking: {item.PosicaoRanking}");
    //        }
    //        Console.WriteLine("*********** FIM *****************");
    //    }

    //    public List<Competidor> ExibeCompetidoresPorRegiaoAgrupados(List<Competidor> listCompetidor)
    //    {
    //        var result = listCompetidor
    //            .OrderBy(c => c.Regiao)
    //            .GroupBy(c => new { c.Regiao, c.MedalhasOuro, c.MedalhasPrata, c.MedalhasBronze, c.Pontos })
    //            .Select(c => new Competidor() {
    //                Regiao = c.Key.Regiao,
    //                Pontos = c.Key.Pontos,
    //                MedalhasOuro = c.Key.MedalhasOuro,
    //                MedalhasPrata = c.Key.MedalhasPrata,
    //                MedalhasBronze = c.Key.MedalhasBronze
    //            });

    //        Console.WriteLine("*********** Exibe Competidores Agrupados Por Região, Medalhas de ouro, prata e bronze *****************");

    //        foreach (var item in result)
    //        {
    //            Console.WriteLine($"{item.Regiao} | Ouro: {item.MedalhasOuro} | Prata: {item.MedalhasPrata} | Bronze: {item.MedalhasBronze}");
    //        }

    //        Console.WriteLine("*********** FIM *****************");
    //        return result.ToList();
    //    }

    //    public int CalculaIdade(DateTime dataNascimento)
    //    {
    //        return (DateTime.Now.Year - dataNascimento.Year);
    //    }

    //    public List<Competidor> CalculaRanking(List<Competidor> listCompetidor)
    //    {
    //        var listCompetidorRanking = listCompetidor.OrderByDescending(c => c.Pontos).ThenBy(c => c.DataUltimaPontuacao).ToList();

    //        for (int i = 0; i < listCompetidorRanking.Count(); i++)
    //        {
    //            if (listCompetidorRanking[i].Pontos >= 10000)
    //                listCompetidorRanking[i].PosicaoRanking = i + 1;
    //        }

    //        return listCompetidorRanking;
    //    }

    //    public List<Competidor> GetCompetidores(List<string> listCompetidores)
    //    {
    //        List<Competidor> list = new List<Competidor>();
    //        int countLine = 1;
    //        foreach (var line in listCompetidores)
    //        {
    //            if (!string.IsNullOrEmpty(line))
    //            {
    //                try
    //                {
    //                    var columns = line.Split(_options.Value.DelimitadorColuna);

    //                    list.Add(new Competidor() {
    //                        DataNascimento = ValidateDate(columns[_options.Value.PosicaoDataNascimento], countLine, "Data Nascimento"),
    //                        IdERP = ValidateText(columns[_options.Value.PosicaoIdERP], countLine, "Idendificação ERP"),
    //                        DataUltimaPontuacao = ValidateDate(columns[_options.Value.PosicaoUltimaOcorrenciaPontuacao], countLine, "Data Ultima Pontuação"),
    //                        Nome = ValidateText(columns[_options.Value.PosicaoNome], countLine, "Nome"),
    //                        Pontos = ValidateInt(columns[_options.Value.PosicaoPontuacao], countLine),
    //                        Regiao = ValidateText(columns[_options.Value.PosicaoRegiao], countLine, "Região")

    //                    });
    //                }
    //                catch (InvalidTextFileLineException ex)
    //                {
    //                    _log.LogError(ex.Message);
    //                }
    //                countLine++;
    //            }
                
    //        }
    //        return list;
    //    }

    //    private int ValidateInt(string value, int linha)
    //    {
    //         if (string.IsNullOrEmpty(value))
    //             throw new InvalidTextFileLineException($"Linha  {linha} - Valor em Pontuação Inválido!");

    //         int result = 0;

    //         if (int.TryParse(value, out result))
    //             return result;

    //         throw new InvalidTextFileLineException($"Linha {linha} - Valor em Pontuação Inválido!");
    //    }

    //    private string ValidateText(string value, int line, string fieldName)
    //    {
    //        if (string.IsNullOrEmpty(value))
    //            throw new InvalidTextFileLineException($"Linha {line}  - Valor em {fieldName} Inválido!");

    //        return value;
    //    }

    //    private DateTime ValidateDate(string value, int line, string fieldName)
    //    {
    //        DateTime dtResult;

    //        if (string.IsNullOrEmpty(value) || !DateTime.TryParse(value, out dtResult))
    //            throw new InvalidTextFileLineException($"Linha {line}  - Valor em {fieldName} Inválido!");

    //        return dtResult;
    //    }

    //    public void ValidaPercentualCompeticao()
    //    {
    //        try
    //        {
    //            if ((_options.Value.NivelA +
    //            _options.Value.NivelB +
    //            _options.Value.NivelC +
    //            _options.Value.NivelD +
    //            _options.Value.NivelE) != 100)
    //                throw new InvalidLevelSettingsException("Percenctual Level dos competidores errado.");
    //        }
    //        catch (InvalidLevelSettingsException ex)
    //        {
    //            _log.LogError(ex.Message);
    //            throw ex;
    //        }
            
    //    }

    //    public void ExibeQuantidadeCompetidores(List<Competidor> listaCompetidores)
    //    {
    //        decimal qtdNivelA, qtdNivelB, qtdNivelC, qtdNivelD, qtdNivelE = 0;

    //        decimal total = listaCompetidores.Count;

    //        qtdNivelA = Math.Round((total * _options.Value.NivelA) / 100);
    //        qtdNivelB = Math.Round((total * _options.Value.NivelB) / 100);
    //        qtdNivelC = Math.Round((total * _options.Value.NivelC) / 100);
    //        qtdNivelD = Math.Round((total * _options.Value.NivelD) / 100);
    //        qtdNivelE = Math.Round((total * _options.Value.NivelE) / 100);

    //        Console.WriteLine("Nível A = " + qtdNivelA);
    //        Console.WriteLine("Nível B = " + qtdNivelB);
    //        Console.WriteLine("Nível C = " + qtdNivelC);
    //        Console.WriteLine("Nível D = " + qtdNivelD);
    //        Console.WriteLine("Nível E = " + qtdNivelE);
    //    }

    //    private string ScapeGenerate(string value)
    //    {
    //        if (value.Length < _options.Value.MaxLenght)
    //        {
    //            int max = _options.Value.MaxLenght - value.Length;
    //            for (int i = 0; i < max; i++)
    //            {
    //                value = value + " ";
    //            }
    //        }

    //        return value;
    //    }
    //}
}
