﻿using Application.Dtos.Response;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public class SaldoApplicationService : ISaldoApplicationService
    {
        private readonly ISaldoDomainService _domainService;
        private readonly IMapper _mapper;
        public SaldoApplicationService(ISaldoDomainService domainService, IMapper mapper)
        {
            _domainService = domainService;
            _mapper = mapper;
        }

        public Task<SaldoResponse> SaldoAtualizado(int idCliente)
        {
            var obj = _domainService.SaldoAtualizado(idCliente).Result;

            return Task.FromResult(_mapper.Map<SaldoResponse>(obj));
        }
    }
}
