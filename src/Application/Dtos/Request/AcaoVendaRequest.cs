﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Dtos.Request
{
    public class AcaoVendaRequest
    {
        public int IdCliente { get; set; }
        public int IdAcao { get; set; }
        public int QtdAcao { get; set; }
        public decimal Valor { get; set; }
    }
}
