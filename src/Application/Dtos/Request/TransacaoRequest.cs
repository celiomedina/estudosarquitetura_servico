﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Dtos.Request
{
    public class TransacaoRequest 
    {
        public int IdCliente { get; set; }
        public int IdTipoTransacao { get; set; }
        public decimal Valor { get; set; }
    }
}
