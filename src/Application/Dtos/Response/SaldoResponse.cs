﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Dtos.Response
{
    public class SaldoResponse
    {
        public int IdCliente { get; set; }
        public decimal SaldoDisponivel { get; set; }
    }
}
