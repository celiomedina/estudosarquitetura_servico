﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.DependenceInjection
{
    public static class Injector
    {
        public static void Register(this IServiceCollection service, IConfiguration config)
        {
            //service.AddTransient<ISysLog, Logger>();
            service.ApplicationServicesIoC(config);
            //service.AddResponse();
        }
    }
}
