﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Domain.Interfaces;
using Repository.Repositories;
using Domain.Services;
using Repository.Proxies;
using Application.Interfaces;
using Application.Processos;
using Application.Services;

namespace Infrastructure.DependenceInjection
{
    public static class ServicesIoC
    {
        public static void ApplicationServicesIoC(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(config["ConnectionStrings:DatabaseConnection"]));            
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped(typeof(IControleAcoesDomainService), typeof(ControleAcoesDomainService));
            services.AddScoped(typeof(IControleTransacoesDomainService), typeof(ControleTransacoesDomainService));
            services.AddScoped(typeof(ICotacaoProxy), typeof(CotacaoProxy));
            services.AddScoped(typeof(IControleAcoesApplicationService), typeof(ControleAcoesApplicationService));
            services.AddScoped(typeof(IControleTransacoesApplicationService), typeof(ControleTransacoesApplicationService));
            services.AddScoped(typeof(ISaldoApplicationService), typeof(SaldoApplicationService));
            services.AddScoped(typeof(ISaldoDomainService), typeof(SaldoDomainService));
            services.AddScoped(typeof(ISaldoRepository), typeof(SaldoRepository));
            services.AddScoped(typeof(IAcaoRepository), typeof(AcaoRepository));
            services.AddScoped(typeof(IAcaoCompraClienteRepository), typeof(AcaoCompraClienteRepository));
            services.AddScoped(typeof(IAcaoVendaClienteRepository), typeof(AcaoVendaClienteRepository));
            services.AddScoped(typeof(IBancoAcaoRepository), typeof(BancoAcaoRepository));
            services.AddScoped(typeof(IClienteRepository), typeof(ClienteRepository));
            services.AddScoped(typeof(ITipoTransacaoRepositoty), typeof(TipoTransacaoRepositoty));
        }
    }
}
