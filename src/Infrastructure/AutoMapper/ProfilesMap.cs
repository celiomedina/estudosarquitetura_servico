﻿using Application.Dtos.Request;
using Application.Dtos.Response;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.AutoMapper
{
    public class ProfilesMap : Profile
    {
        public ProfilesMap()
        {
            CreateMap<AcaoCompraCliente, AcaoCompraRequest>()
                .ReverseMap();

            CreateMap<AcaoVendaCliente, AcaoVendaRequest>()
                .ReverseMap();

            CreateMap<Saldo, SaldoResponse>()
                .ReverseMap();
        }
    }
}
