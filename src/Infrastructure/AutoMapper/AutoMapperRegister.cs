﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.AutoMapper
{
    public class AutoMapperRegister
    {
        public static IMapper Mapper { get; set; }

        public static void AutomapperConfig()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProfilesMap());
            });

            Mapper = mappingConfig.CreateMapper();
        }

        public static void RegisterServices(IServiceCollection services)
        {
            AutomapperConfig();
            services.AddSingleton(Mapper);
        }
    }
}
