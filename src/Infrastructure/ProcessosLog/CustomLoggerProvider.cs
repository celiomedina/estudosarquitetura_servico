﻿using Domain.AppSettings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Concurrent;

namespace Infra.ProcessosLog
{
    public class CustomLoggerProvider : ILoggerProvider
    {
        readonly CustomLoggerProviderConfiguration loggerConfig;
        readonly ConcurrentDictionary<string, CustomLogger> loggers = 
            new ConcurrentDictionary<string, CustomLogger>();

        private readonly IOptions<ArquivoCompetidoresSettings> _options;

        public CustomLoggerProvider(CustomLoggerProviderConfiguration config, IOptions<ArquivoCompetidoresSettings> options)
        {
            loggerConfig = config;
            _options = options;
        }

        public ILogger CreateLogger(string category)
        {
            return loggers.GetOrAdd(category,name => new CustomLogger(name, loggerConfig, _options));
        }

        public void Dispose()
        {
            //codigo para liberar os recursos
        }
    }
}
