﻿using Domain.AppSettings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;

namespace Infra.ProcessosLog
{
    public class CustomLogger : ILogger
    {
        readonly string loggerName;
        readonly CustomLoggerProviderConfiguration loggerConfig;
        private readonly IOptions<ArquivoCompetidoresSettings> _options;
        public CustomLogger(string name, CustomLoggerProviderConfiguration config, IOptions<ArquivoCompetidoresSettings> options)
        {
            this.loggerName = name;
            loggerConfig = config;
            _options = options;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, 
            Exception exception, Func<TState, Exception, string> formatter)
        {
            string mensagem = $"{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")} | {formatter(state, exception)}";
                
                //string.Format("{0}: {1} - {2}", logLevel.ToString(), 
                //eventId.Id, formatter(state, exception));
            EscreverTextoNoArquivo(mensagem);
        }
        private void EscreverTextoNoArquivo(string mensagem)
        {
            string caminhoArquivoLog = _options.Value.pathLog;
            using (StreamWriter file = new StreamWriter(caminhoArquivoLog, true))
            {
                file.WriteLine(mensagem);
                file.Close();
            }
        }
    }
}
