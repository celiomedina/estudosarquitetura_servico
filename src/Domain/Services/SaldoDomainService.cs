﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public class SaldoDomainService : ISaldoDomainService
    {
        private readonly ISaldoRepository _saldoRepository;
        public SaldoDomainService(ISaldoRepository saldoRepository)
        {
            _saldoRepository = saldoRepository;
        }

        Task<Saldo> ISaldoDomainService.SaldoAtualizado(int idCliente)
        {
            var result = _saldoRepository.ObterPorIdAsync(idCliente);

            return result;
        }
    }
}
