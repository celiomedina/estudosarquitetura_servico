﻿using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class ControleAcoesDomainService : IControleAcoesDomainService
    {
        private readonly IAcaoCompraClienteRepository _acaoCompraRepository;
        private readonly IAcaoVendaClienteRepository _acaoVendaRepository;
        private readonly IAcaoRepository _acaoRepository;
        private readonly ISaldoRepository _saldoRepository;
        private readonly IBancoAcaoRepository _bancoAcaoRepository;
        private readonly ICotacaoProxy _proxy;
        public ControleAcoesDomainService(IAcaoCompraClienteRepository acaoCompraRepository,
            IAcaoVendaClienteRepository acaoVendaRepository,
            IAcaoRepository acaoRepository,
            ISaldoRepository saldoRepository,
            IBancoAcaoRepository bancoAcaoRepository,
            ICotacaoProxy proxy)
        {
            _acaoCompraRepository = acaoCompraRepository;
            _acaoVendaRepository = acaoVendaRepository;
            _acaoRepository = acaoRepository;
            _saldoRepository = saldoRepository;
            _bancoAcaoRepository = bancoAcaoRepository;
            _proxy = proxy;
        }

        public Task<bool> AcaoCompra(AcaoCompraCliente request)
        {
            var objBancoAcoes = _bancoAcaoRepository.ObterPorIdAsync(request.IdAcao).Result;

            if (objBancoAcoes == null || (objBancoAcoes != null && objBancoAcoes.QtdAcao <= 0))
                throw new Exception("Sem ações para compra!");

            var saldoCliente = _saldoRepository.ObterPorIdAsync(request.IdCliente).Result;

            if (saldoCliente != null && saldoCliente.SaldoDisponivel >= (request.QtdAcao * request.Valor))
            {
                var objCriaCompraAcao = _acaoCompraRepository.CriarAsync(request).Result;

               if (objCriaCompraAcao.Id > 0)
                {
                    var saldoAtual = saldoCliente.SaldoDisponivel - (request.QtdAcao * request.Valor);
                    saldoCliente.SaldoDisponivel = saldoAtual;
                    if (_saldoRepository.AtualizarAsync(saldoCliente.Id, saldoCliente).Result.SaldoDisponivel == saldoAtual)
                    {
                        objBancoAcoes.QtdAcao = objBancoAcoes.QtdAcao - request.QtdAcao;
                        var objBancoAcao = _bancoAcaoRepository.AtualizarAsync(objBancoAcoes.Id, objBancoAcoes);
                        if (objBancoAcao is null)
                            throw new Exception("Erro na atualização do banco de ações!");

                        return Task.FromResult(true);
                    }
                    else
                    {
                        _acaoCompraRepository.ExcluirAsync(objCriaCompraAcao.Id);
                        return Task.FromResult(false);
                    }
                }
            }

            return Task.FromResult(false);
        }

        public Task<bool> AcaoVenda(AcaoVendaCliente request)
        {
            var objBancoAcoesCLiente = _acaoCompraRepository.ObterPorClienteAsync(request.IdCliente).Result;

            if (objBancoAcoesCLiente == null || (objBancoAcoesCLiente != null && objBancoAcoesCLiente.QtdAcao < request.QtdAcao))
                throw new Exception("Ações insuficientes para venda!");
            
            var objVendaCompraAcao = _acaoVendaRepository.CriarAsync(request).Result;

            var saldoCliente = _saldoRepository.ObterPorIdAsync(request.IdCliente).Result;

            if (objVendaCompraAcao.Id > 0)
            {
                var saldoAtual = saldoCliente.SaldoDisponivel + (request.QtdAcao * request.Valor);
                saldoCliente.SaldoDisponivel = saldoAtual;
                if (_saldoRepository.AtualizarAsync(saldoCliente.Id, saldoCliente).Result.SaldoDisponivel == saldoAtual)
                {
                    var objBancoAcoes = _bancoAcaoRepository.ObterPorIdAsync(request.IdAcao).Result;
                    objBancoAcoes.QtdAcao = objBancoAcoes.QtdAcao + request.QtdAcao;
                    var objBancoAcao = _bancoAcaoRepository.AtualizarAsync(objBancoAcoes.Id, objBancoAcoes);
                    if (objBancoAcao is null)
                        throw new Exception("Erro na atualização do banco de ações!");

                    return Task.FromResult(true);
                }
                else
                {
                    _acaoCompraRepository.ExcluirAsync(objVendaCompraAcao.Id);
                    return Task.FromResult(false);
                }
            }           

            return Task.FromResult(false);
        }
    }
}
