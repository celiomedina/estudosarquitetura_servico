﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public class ControleTransacoesDomainService : IControleTransacoesDomainService
    {
        public Task<bool> Saque(Transacao request) { return null; }

        public Task<bool> Deposito(Transacao request) { return null; }
    }
}
