﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRepository<TEntidade, TEntidadeId>
        where TEntidade : Entity<TEntidadeId>
        where TEntidadeId : struct
    {
        Task<TEntidade> ObterPorIdAsync(TEntidadeId id);
        Task<TEntidade> CriarAsync(TEntidade entidade);
        Task<TEntidade> AtualizarAsync(TEntidadeId id, TEntidade entidadeAtualizar);
        Task ExcluirAsync(TEntidadeId id);
    }
}
