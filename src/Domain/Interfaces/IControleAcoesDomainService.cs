﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IControleAcoesDomainService
    {
        Task<bool> AcaoCompra(AcaoCompraCliente request);

        Task<bool> AcaoVenda(AcaoVendaCliente request);
    }
}
