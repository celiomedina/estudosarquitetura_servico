﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface ICotacaoProxy
    {
        decimal Calcula(decimal value);
    }
}
