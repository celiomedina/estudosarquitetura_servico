﻿using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IAcaoCompraClienteRepository : IRepository<AcaoCompraCliente, int>
    {
        Task<AcaoCompraCliente> ObterPorClienteAsync(int idCliente);
    }
}
