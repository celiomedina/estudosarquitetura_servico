﻿using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IAcaoVendaClienteRepository : IRepository<AcaoVendaCliente, int>
    {
    }
}
