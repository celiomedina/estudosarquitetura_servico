﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.AppSettings
{
    public class ArquivoCompetidoresSettings
    {
        public string DelimitadorColuna { get; set; }
        public string DelimitadorLinha { get; set; }
        public int MaxLenght { get; set; }
        public int PosicaoIdERP { get; set; }
        public int PosicaoNome { get; set; }
        public int PosicaoDataNascimento { get; set; }
        public int PosicaoPontuacao { get; set; }
        public int PosicaoRegiao { get; set; }
        public int PosicaoUltimaOcorrenciaPontuacao { get; set; }
        public string pathReadERPCompetidores { get; set; }
        public string pathWriteJsonCompetidores { get; set; }
        public string pathLog { get; set; }
        public decimal NivelA { get; set; }
        public decimal NivelB { get; set; }
        public decimal NivelC { get; set; }
        public decimal NivelD { get; set; }
        public decimal NivelE { get; set; }
    }
}
