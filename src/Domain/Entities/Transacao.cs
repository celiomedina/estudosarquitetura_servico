﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Transacao : Entity<int>
    {
        public int IdCliente { get; set; }
        public int IdTipoTransacao { get; set; }
        public decimal Valor { get; set; }

    }
}
