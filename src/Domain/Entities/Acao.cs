﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Acao : Entity<int>
    {
        public string Nome { get; set; }
        public decimal Valor { get; set; }
    }
}
