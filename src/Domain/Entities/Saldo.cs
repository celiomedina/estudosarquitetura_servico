﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Saldo : Entity<int>
    {
        public int IdCliente { get; set; }
        public decimal SaldoDisponivel { get; set; }
    }
}
