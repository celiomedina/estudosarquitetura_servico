﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class BancoAcao : Entity<int>
    {
        public int IdAcao { get; set; }
        public int QtdAcao { get; set; }

    }
}
