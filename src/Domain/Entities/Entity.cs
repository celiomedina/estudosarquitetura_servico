﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public abstract class Entity<TId> where TId : struct
    {
        public TId Id { get; set; }
    }

}
