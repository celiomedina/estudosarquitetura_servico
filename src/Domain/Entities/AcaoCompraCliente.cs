﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class AcaoCompraCliente : Entity<int>
    {
        public int IdCliente { get; set; }
        public int IdAcao { get; set; }
        public decimal Valor { get; set; }
        public int QtdAcao { get; set; }
    }
}
