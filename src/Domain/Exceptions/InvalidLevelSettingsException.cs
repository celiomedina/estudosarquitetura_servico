﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Exceptions
{
    public class InvalidLevelSettingsException : Exception
    {
        public InvalidLevelSettingsException(string message) : base(message)
        {

        }
    }
}
