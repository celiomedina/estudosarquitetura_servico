﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Exceptions
{
    public class InvalidTextFileLineException : Exception
    {
        public InvalidTextFileLineException(string message) : base(message)
        {

        }
    }
}
