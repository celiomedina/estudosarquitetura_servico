﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.DataBase.Configuration
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Acao> Acao { get; set; }
        public virtual DbSet<AcaoCompraCliente> AcaoCompraCliente { get; set; }
        public virtual DbSet<AcaoVendaCliente> AcaoVendaCliente { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Saldo> Saldo { get; set; }
        public virtual DbSet<TipoTransacao> TipoTransacao { get; set; }
        public virtual DbSet<Transacao> Transacao { get; set; }
        public virtual DbSet<BancoAcao> BancoAcao { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Acao>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<AcaoCompraCliente>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<AcaoVendaCliente>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<Saldo>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<TipoTransacao>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<Transacao>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            modelBuilder.Entity<BancoAcao>(entity => 
            {
                entity.Property(e => e.Id).IsRequired();
            });
        }
    }
}
