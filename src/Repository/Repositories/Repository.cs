﻿
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class Repository<TEntidade, TEntidadeId> : IRepository<TEntidade, TEntidadeId>
        where TEntidade : Entity<TEntidadeId>
        where TEntidadeId : struct
    {
        protected readonly ApplicationDbContext DbContext;
        protected readonly DbSet<TEntidade> dbSet;

        public Repository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
            dbSet = dbContext.Set<TEntidade>();
        }

        public virtual Task<TEntidade> ObterPorIdAsync(TEntidadeId id)
        {
            return dbSet.FindAsync(id);
        }

        public virtual async Task<TEntidade> CriarAsync(TEntidade entidade)
        {
            await dbSet.AddAsync(entidade);
            await DbContext.SaveChangesAsync();
            return entidade;
        }


        public virtual async Task<TEntidade> AtualizarAsync(TEntidadeId id, TEntidade entidadeAtualizar)
        {
            var entityDb = await ObterPorIdAsync(id);
            ValidarEntidade(entityDb);
            entidadeAtualizar.Id = entityDb.Id;
            DbContext.Entry(entityDb).Property(x => x.Id).IsModified = false;
            DbContext.Entry(entityDb).CurrentValues.SetValues(entidadeAtualizar);
            await DbContext.SaveChangesAsync();
            return entityDb;
        }

        public virtual async Task ExcluirAsync(TEntidadeId id)
        {
            var entityDb = await ObterPorIdAsync(id);
            ValidarEntidade(entityDb);
            DbContext.Remove(entityDb);
            await DbContext.SaveChangesAsync();
        }

        public async Task<bool> ExistePorIdAsync(TEntidadeId id)
        {
            return await dbSet.AnyAsync(entityDb => entityDb.Id.Equals(id));
        }

        private void ValidarEntidade(TEntidade entidade)
        {
            if (entidade == null)
            {
                throw new Exception(
                    $"{typeof(TEntidade).Name} não encontrado(a).");
            }
        }
    }
}
