﻿using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public class AcaoVendaClienteRepository : Repository<AcaoVendaCliente, int>, IAcaoVendaClienteRepository
    {
        public AcaoVendaClienteRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
