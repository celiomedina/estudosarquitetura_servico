﻿using Domain.Entities;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public class SaldoRepository : Repository<Saldo, int>, ISaldoRepository
    {
        public SaldoRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
