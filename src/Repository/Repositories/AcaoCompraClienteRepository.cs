﻿using Domain.Entities;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Infrastructure.DataBase.Configuration;

namespace Repository.Repositories
{
    public class AcaoCompraClienteRepository : Repository<AcaoCompraCliente, int>, IAcaoCompraClienteRepository
    {
        protected readonly DbSet<AcaoCompraCliente> DbSet;

        public AcaoCompraClienteRepository(ApplicationDbContext dbContext) : base (dbContext)
        {
            DbSet = dbContext.Set<AcaoCompraCliente>();
        }

        public Task<AcaoCompraCliente> ObterPorClienteAsync(int idCliente)
        {
            return Task.FromResult(DbSet.Where(c => c.IdCliente == idCliente).FirstOrDefault());
        }
    }
}
