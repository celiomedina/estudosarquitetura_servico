﻿using Domain.Entities;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public class TipoTransacaoRepositoty : Repository<TipoTransacao, int>, ITipoTransacaoRepositoty
    {
        public TipoTransacaoRepositoty(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
