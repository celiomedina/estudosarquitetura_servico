﻿using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class AcaoRepository : Repository<Acao, int>,  IAcaoRepository
    {
        public AcaoRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
