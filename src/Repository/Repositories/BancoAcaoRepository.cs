﻿using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class BancoAcaoRepository : Repository<BancoAcao, int>, IBancoAcaoRepository
    {
        public BancoAcaoRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
