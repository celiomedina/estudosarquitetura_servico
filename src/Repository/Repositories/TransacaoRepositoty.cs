﻿using Domain.Entities;
using Infrastructure.DataBase.Configuration;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public class TransacaoRepositoty : Repository<Transacao, int>, ITransacaoRepositoty
    {
        public TransacaoRepositoty(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
