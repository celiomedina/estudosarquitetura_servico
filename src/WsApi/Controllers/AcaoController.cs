﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Dtos.Request;
using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WsApi.Controllers
{
    public class AcaoController : Controller
    {
        private readonly IControleAcoesApplicationService _acaoService;

        public AcaoController(IControleAcoesApplicationService acaoService)
        {
            _acaoService = acaoService;
        }

        [HttpPost]
        [Route("CompraAcao")]
        public async Task<ActionResult> PostCompraAcao(AcaoCompraRequest request)
        {
            var objResult = _acaoService.AcaoCompra(request);

            if (objResult.IsCompleted)
                return Ok(objResult.Result);

            return NotFound();
        }

        [HttpPost]
        [Route("VendaAcao")]
        public async Task<ActionResult> PostVendaAcao(AcaoVendaRequest request)
        {
            var objResult = _acaoService.AcaoVenda(request);

            if (objResult.IsCompleted)
                return Ok(objResult.Result);

            return NotFound();
        }
    }
}