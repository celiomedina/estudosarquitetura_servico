﻿using Application.Dtos.Response;
using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WsApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaldoController : Controller
    {
        private readonly ISaldoApplicationService _saldoService;

        public SaldoController(ISaldoApplicationService saldoService)
        {
            _saldoService = saldoService;
        }

        [HttpGet]
        [Route("{idCliente}")]
        public async Task<ActionResult<SaldoResponse>> GetSaldo([FromRoute]int idCliente)
        {
            var objResult = await _saldoService.SaldoAtualizado(idCliente);

            if (objResult != null)
                return Ok(objResult);

            return NotFound();
        }
    }
}