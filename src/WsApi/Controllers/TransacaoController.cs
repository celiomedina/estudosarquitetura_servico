﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Dtos.Request;
using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WsApi.Controllers
{
    public class TransacaoController : Controller
    {
        private readonly IControleTransacoesApplicationService _transacaoService;

        public TransacaoController(IControleTransacoesApplicationService acaoService)
        {
            _transacaoService = acaoService;
        }

        [HttpPost]
        [Route("Deposito")]
        public async Task<ActionResult> PostDeposito(TransacaoRequest request)
        {
            var objResult = _transacaoService.Deposito(request);

            if (objResult.IsCompleted)
                return Ok(objResult.Result);

            return NotFound();
        }

        [HttpPost]
        [Route("Saque")]
        public async Task<ActionResult> PostSaque(TransacaoRequest request)
        {
            var objResult = _transacaoService.Saque(request);

            if (objResult.IsCompleted)
                return Ok(objResult.Result);

            return NotFound();
        }

    }
}